import React from 'react'
import {connect} from 'react-redux'
import ClientList from './ClientList'
import ClientInfo from './ClientInfo'

class App extends React.Component {

   searchClient = () => {
    const{clients} = this.props.state
    let str = this.inputVal.value.toLowerCase();
    let foundClient = clients.filter(client=>{
        let strClient = JSON.stringify(client).toLowerCase();
        return strClient.indexOf(str) !== -1
    })
    console.log(foundClient)
    if(foundClient) {
        
        this.props.onSearch(foundClient)
    }
   }
    render() {
        return(
            <div className='container wrap'>
                <div className="row">
                    <div className="col left p-0">
                        <div className='search'>
                            <input type="text" onInput={this.searchClient.bind(this)} ref={(input) =>{this.inputVal = input}}/>
                        </div>
                         <ClientList />
                    </div>
                    <div className="col right ml-3">
                         <ClientInfo />
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(
    state =>({state: state}),
    dispatch =>({
        onSearch: searchedClient => {
            dispatch({type: 'ADD_FOUND', foundClients: searchedClient})
        }
    })
)(App)