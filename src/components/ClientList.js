import React from 'react'
import {connect} from 'react-redux'
import Client from './Client'

function ClientList(props){
    const {clients} = props.state
    const {foundClients} = props.state
    if(foundClients !== undefined && foundClients.length > 0)
    return (
        <ul>
        {
            foundClients.map((client, index) => {
                return <Client key={index} client={client}/>
            })
        }  
    </ul>  
    )
    return (
        <ul>
            {
                clients.map((client, index) => {
                    return <Client key={index} client={client}/>
                })
            }  
        </ul>
    )
}
export default connect(
    state=>({
        state: state
    }),
    dispatch=>({

    })
)(ClientList)