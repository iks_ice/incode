import React from 'react'
import {connect} from 'react-redux'

function ClientInfo(props){
    const{avatar,firstName, lastName}   = props.state.selectedClient.general 
    const{company,title}                = props.state.selectedClient.job 
    const{email,phone}                  = props.state.selectedClient.contact 
    const{street,city, zipCode,country} = props.state.selectedClient.address 
    return (

        <div className="clientinfo">
            <div className="row">
                <div className="col-4">
                    <img src={avatar} alt=""/>
                </div>
                <div className="col-8">
                    <h4>{`${firstName} ${lastName}`}</h4>
                    <h6>{`${title} ${company}`}</h6> 
                </div>
            </div>
            <div className="row">
                <div className="col">
                    <p>Email:{` ${email}`}</p>
                    <p>Phone:{` ${phone}`}</p>
                </div>
                <div className="col">
                    <p>Street:{ `${street}`}</p>
                    <p>City:{` ${city}`}</p>
                    <p>Zip code:{` ${zipCode}`}</p>
                    <p>Country:{` ${country}`}</p>

                </div>
            </div>
        </div>
       
    )
}
export default connect(
    state => ({
        state: state
    })
)(ClientInfo)