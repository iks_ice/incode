import React from 'react'
import {connect} from 'react-redux'

class Client extends React.Component{
    selectClient = () => {
        this.props.onAddClient(this.props.client)
    }
    render() {
        const{firstName, lastName, avatar} = this.props.client.general
        const{title}        = this.props.client.job
        return (
            <div className="container">
                <li className='client row' onClick={this.selectClient.bind(this)}>
                    <div className="col-2 p-0 ">
                        <img src={avatar} alt=""/>  
                    </div>
                    <div className="col-10 p-0">
                        <p>{` ${firstName} ${lastName}`}</p>
                        <span>{title}</span>
                    </div>
                </li>
            </div>
            
        )
    }
}
export default connect(
    state => ({
        state: state
    }),
    dispatch => ({
        onAddClient: (selectedClient) => {
            dispatch({type:'SELECT_CLIENT', selectedClient: selectedClient})
        }
    })
)(Client)