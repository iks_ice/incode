import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
import App from './components/App'
import 'bootstrap/dist/css/bootstrap.min.css'
import './components/index.css'
import clients from './clients.json'

const initialState = {
    clients:        clients,
    selectedClient: clients[0],
    foundClients: []
}

function list(state = initialState, action) {
        switch(action.type){
        case 'SELECT_CLIENT':
        return {
            clients:        clients,
            selectedClient: action.selectedClient,
            foundClients: state.foundClients
        }
        case 'ADD_FOUND':
        return{
            clients:        clients,
            selectedClient: clients[0],
            foundClients:  action.foundClients
        }
        default: return state
    }
}

const store = createStore(list, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

ReactDOM.render(
<Provider store={store} >
    <App />
</Provider>, document.getElementById('root'))